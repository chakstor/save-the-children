$(document).ready(function() {

    //  ------------------------------
    //    Main Navigation
    //  ------------------------------
    var lastId;
    var topMenu = $("#main-header");
    var topMenuHeight = topMenu.outerHeight();

    // Todos los items del header
    var menuItems = topMenu.find("a");

    var scrollItems = menuItems.map(function() {
        var value = $(this).attr("href");

        if ( value.charAt(0) == "#" ) {
            var item = $( $(this).attr("href") );
            if (item.length) {
                return item;
            }
        }
    });

    // Al hacer click en cada opción del menú
    menuItems.click(function(e) {
        var href = $(this).attr("href");

        if ( href.charAt(0) == "#" ) {
            e.preventDefault();

            var offsetTop = href === "#" ? 0 : $(href).offset().top-1;

            $('html, body').stop().animate({
                scrollTop: offsetTop
            }, 1000);
        }

        if($(window).width() < 992) {
            $("#nav-button button").trigger("click");
        }
    });

    // Al hacer scroll en toda la página
    $(window).scroll(function() {
        var fromTop = $(this).scrollTop()+topMenuHeight;

        var cur = scrollItems.map(function(){
            if ($(this).offset().top < fromTop)
                return this;
        });

        cur = cur[cur.length-1];
        var id = cur && cur.length ? cur[0].id : "";

        if (lastId !== id) {
            lastId = id;
            menuItems
                .parent().removeClass("active")
                .end().filter("[href=#"+id+"]").parent().addClass("active");
        }
    });



    //  ------------------------------
    //    Mobile Main Nav Button
    //  ------------------------------
    $("#navigation-btn").on("click", function(){
        $("#main-navigation ul").slideToggle();
    });



    //  ------------------------------
    //    Side Slide
    //  ------------------------------
    var somosSlider = $('#somos-horizontal-scroll').bxSlider({
        auto: false,
        autoControls: false,
        pager: false,
        touchEnabled: true,
        nextSelector: '#somos-next',
        prevSelector: '#somos-prev',
        nextText: '<i class="fa fa-caret-right"></i>',
        prevText: '<i class="fa fa-caret-left"></i>'
    });

    var publicacionesSlider = $('#publicaciones-horizontal-scroll').bxSlider({
        auto: false,
        autoControls: false,
        pager: false,
        touchEnabled: true,
        nextSelector: '#publicaciones-next',
        prevSelector: '#publicaciones-prev',
        nextText: '<i class="fa fa-caret-right"></i>',
        prevText: '<i class="fa fa-caret-left"></i>'
    });

    var uneteSlider = $('#unete-horizontal-scroll').bxSlider({
        auto: false,
        autoControls: false,
        pager: false,
        touchEnabled: true,
        nextSelector: '#unete-next',
        prevSelector: '#unete-prev',
        nextText: '<i class="fa fa-caret-right"></i>',
        prevText: '<i class="fa fa-caret-left"></i>'
    });



    //  ------------------------------
    //    Aliados Tabs
    //  ------------------------------
    $("#aliados .vertical-tabset a").on("click", function(e) {
        e.preventDefault();
        var href = $(this).attr("href");

        $(this).parent("li").addClass("active").siblings().removeClass("active");
        $(href).siblings(":visible").stop().fadeOut('100', function() {
            $(href).stop().fadeIn('200');
        });
    });



    //  ------------------------------
    //    Galería
    //  ------------------------------
    $("#gallery-menu a, #galeria .tabset a").on("click", function(e) {
        e.preventDefault();
        var href = $(this).attr("href");

        $("#galeria .tabset a[href='"+href+"']").parent("li").addClass("active").siblings().removeClass("active");

        if ( $("#gallery-menu").is(":visible") ) {
            $("#gallery-menu").stop().fadeOut('100', function() {
                $("#galeria "+href).stop().fadeIn('200');
            });
        } else {
            $(href).siblings(":visible").stop().fadeOut('100', function() {
                $(href).stop().fadeIn('200');
            });
        }
    });



    //  ------------------------------
    //    Fancybox
    //  ------------------------------
    $(".fancy").fancybox();



    //  ------------------------------
    //    Modals
    //  ------------------------------
    $("a").on("click", function(e) {
        if($(this).data("modal-trigger")) {
            e.preventDefault();
            $($(this).attr('href')).fadeToggle();
        }
    });

    $(".stc-modal-close").on("click", function(e) {
        e.preventDefault();
        $(this).parent(".stc-modal").fadeOut();
    });



    //  ------------------------------
    //    Main Nav Fix
    //  ------------------------------
    if($(window).width > 992) {
        mainNavFix();
    }
});

$(window).resize(function() {
    mainNavFix();
});

function mainNavFix() {
    $("#main-navigation ul").removeAttr("style");
}


//  ------------------------------
//   Scroller
//  ------------------------------

(function($){
    $(window).load(function(){
        $(".scroller-content").mCustomScrollbar();
    });
})(jQuery);
